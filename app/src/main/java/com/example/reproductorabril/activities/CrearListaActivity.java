package com.example.reproductorabril.activities;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reproductorabril.R;
import com.example.reproductorabril.db.DBHelper;

public class CrearListaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_lista);

        DBHelper db = new DBHelper(this, "reproductor", null, 1);

        EditText nombreEdit = findViewById(R.id.nombreEditText);

        Button btn_newlista = findViewById(R.id.btn_newlista);
        btn_newlista.setOnClickListener(view -> {
            String nombre = nombreEdit.getText().toString().trim();
            if (!nombre.isEmpty()) {
                db.createLista(nombre);
                Toast.makeText(getApplicationContext(), R.string.toast_add_lista, Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}