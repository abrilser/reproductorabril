package com.example.reproductorabril.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reproductorabril.R;
import com.example.reproductorabril.db.DBHelper;
import com.example.reproductorabril.modelo.Cancion;
import com.example.reproductorabril.modelo.Lista;

import java.util.ArrayList;
import java.util.List;

public class AddListaActivity extends AppCompatActivity {

    List<Lista> listas = new ArrayList<>();
    AdaptadorListas adaptador;
    DBHelper db;
    Cancion cancion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lista);
        Bundle ex = getIntent().getExtras();
        cancion = ex.getParcelable("cancion");
        db = new DBHelper(this, "reproductor", null, 1);
        listas.addAll(db.getListas(this));
        adaptador = new AdaptadorListas(this);
        ListView lista = findViewById(R.id.lista_listas2);
        lista.setAdapter(adaptador);
    }

    class AdaptadorListas extends ArrayAdapter {
        final Activity context;

        public AdaptadorListas(Activity context) {
            super(context, R.layout.listitem_lista2, listas);
            this.context = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Lista lista = listas.get(position);
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.listitem_lista2, parent, false);
            TextView lbl_lista2 = item.findViewById(R.id.lbl_lista2);
            lbl_lista2.setText(lista.getNombre());
            Button btn_add = item.findViewById(R.id.btn_add_this_lista);
            btn_add.setOnClickListener(view -> {
                db.addCancionToLista(cancion.getUri(), lista.getId());
                Toast.makeText(getApplicationContext(), R.string.toast_add_cancion, Toast.LENGTH_SHORT).show();
                finish();
            });
            return item;
        }
    }
}