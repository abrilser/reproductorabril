package com.example.reproductorabril.activities;

import static com.example.reproductorabril.R.drawable.ic_baseline_music_note_24;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reproductorabril.R;
import com.example.reproductorabril.modelo.Cancion;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PlayActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener {

    private static final MediaPlayer mediaPlayer = new MediaPlayer();
    private static String dataSrc = "";
    private final Handler mHandler = new Handler();
    Cancion cancion;
    SeekBar seekbar;
    List<Cancion> canciones;
    int position;
    long listaid;
    TextView play_cancion;
    TextView play_artista;
    TextView play_album;
    TextView play_duracion;
    TextView play_duracion2;
    private final Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = mediaPlayer.getDuration();
            long currentDuration = mediaPlayer.getCurrentPosition();
            int progress = getProgressPercentage(currentDuration, totalDuration);
            seekbar.setProgress(progress);
            long minutes = TimeUnit.MILLISECONDS.toMinutes(currentDuration);
            long seconds = TimeUnit.MILLISECONDS.toSeconds(currentDuration) - TimeUnit.MINUTES.toSeconds(minutes);
            play_duracion2.setText(minutes + ":" + (seconds > 9 ? seconds : "0" + seconds));
            mHandler.postDelayed(this, 100);
        }
    };
    ImageView song_icon;
    Button btn_play;
    Button btn_pause;
    Button btn_backward;
    Button btn_forward;
    Button btn_previous;
    Button btn_next;

    private volatile boolean playing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        Bundle ex = getIntent().getExtras();

        cancion = ex.getParcelable("cancion");
        canciones = ex.getParcelableArrayList("canciones");
        position = ex.getInt("position");
        listaid = ex.getLong("lista");

        mediaPlayer.setOnCompletionListener(this);

        play_cancion = findViewById(R.id.play_cancion);
        play_artista = findViewById(R.id.play_artista);
        play_album = findViewById(R.id.play_album);
        play_duracion = findViewById(R.id.play_duracion);
        play_duracion2 = findViewById(R.id.play_duracion2);
        song_icon = findViewById(R.id.play_song_icon);
        seekbar = findViewById(R.id.seekBar);
        btn_play = findViewById(R.id.btn_play);
        btn_pause = findViewById(R.id.btn_pause);
        btn_backward = findViewById(R.id.btn_backward);
        btn_forward = findViewById(R.id.btn_forward);
        btn_previous = findViewById(R.id.btn_previous);
        btn_next = findViewById(R.id.btn_next);

        seekbar.setClickable(false);
        seekbar.setOnSeekBarChangeListener(this);

        seekbar.setMax(100);

        updateToSong();
        updateProgressBar();

        btn_play.setOnClickListener(view -> {
            btn_play.setVisibility(View.GONE);
            btn_pause.setVisibility(View.VISIBLE);
            mediaPlayer.start();
            playing = true;
        });

        btn_pause.setOnClickListener(view -> {
            btn_pause.setVisibility(View.GONE);
            btn_play.setVisibility(View.VISIBLE);
            mediaPlayer.pause();
            playing = false;
        });

        btn_backward.setOnClickListener(view -> {
            mHandler.removeCallbacks(mUpdateTimeTask);
            int totalDuration = mediaPlayer.getDuration();
            int currentPosition = progressToTimer(seekbar.getProgress() - 5, totalDuration);
            mediaPlayer.seekTo(currentPosition);
            updateProgressBar();
        });

        btn_forward.setOnClickListener(view -> {
            mHandler.removeCallbacks(mUpdateTimeTask);
            int totalDuration = mediaPlayer.getDuration();
            int currentPosition = progressToTimer(seekbar.getProgress() + 5, totalDuration);
            mediaPlayer.seekTo(currentPosition);
            updateProgressBar();
        });

        btn_next.setOnClickListener(view -> onCompletion(mediaPlayer));

        btn_previous.setOnClickListener(view -> {
            position = position - 1 < 0 ? canciones.size() - 1 : position - 1;
            cancion = canciones.get(position);
            updateToSong();
        });

        Button btn_gotolistas = findViewById(R.id.btn_addplay);
        btn_gotolistas.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), AddListaActivity.class);
            intent.putExtra("cancion", cancion);
            startActivity(intent);
        });
    }

    public void updateToSong() {
        play_cancion.setText(cancion.getNombre().isEmpty() ? "Canción sin título" : cancion.getNombre());
        play_artista.setText(cancion.getArtista().isEmpty() ? "Artista desconocido" : cancion.getArtista());
        play_album.setText(cancion.getAlbum().isEmpty() ? "Album desconocido" : cancion.getAlbum());
        play_duracion.setText(cancion.getDuracion());

        if (cancion.getCover() != null) {
            song_icon.setImageBitmap(cancion.getCover());
            song_icon.setPadding(0, 0, 0, 0);
            song_icon.setClipToOutline(true);
        } else {
            song_icon.setImageResource(ic_baseline_music_note_24);
            song_icon.setPadding(50, 50, 50, 50);
        }

        if (!dataSrc.equals(cancion.getUri())) {
            try {
                mediaPlayer.reset();
                mediaPlayer.setDataSource(cancion.getUri());
                mediaPlayer.prepare();
                dataSrc = cancion.getUri();
            } catch (IOException e) {
                e.printStackTrace();
            }
            seekbar.setProgress(0);
            if (playing) {
                btn_play.setVisibility(View.GONE);
                btn_pause.setVisibility(View.VISIBLE);
                mediaPlayer.start();
            } else {
                btn_pause.setVisibility(View.GONE);
                btn_play.setVisibility(View.VISIBLE);
            }
        } else if (mediaPlayer.isPlaying()) {
            btn_pause.setVisibility(View.VISIBLE);
            btn_play.setVisibility(View.GONE);
        }

        SharedPreferences sharedPref = getSharedPreferences("key", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("position", position);
        editor.putLong("lista", listaid);
        editor.apply();
    }

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    public int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;
        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);
        percentage = (((double) currentSeconds) / totalSeconds) * 100;
        return percentage.intValue();
    }

    public int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = totalDuration / 1000;
        currentDuration = (int) ((((double) progress) / 100) * totalDuration);
        return currentDuration * 1000;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        position = canciones.size() == position + 1 ? 0 : position + 1;
        cancion = canciones.get(position);
        updateToSong();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mediaPlayer.getDuration();
        int currentPosition = progressToTimer(seekBar.getProgress(), totalDuration);
        mediaPlayer.seekTo(currentPosition);
        updateProgressBar();
    }
}