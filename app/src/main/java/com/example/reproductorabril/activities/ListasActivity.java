package com.example.reproductorabril.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reproductorabril.R;
import com.example.reproductorabril.db.DBHelper;
import com.example.reproductorabril.modelo.Lista;

import java.util.ArrayList;
import java.util.List;

public class ListasActivity extends AppCompatActivity {

    List<Lista> listas = new ArrayList<>();
    AdaptadorListas adaptador;
    DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listas);
        db = new DBHelper(this, "reproductor", null, 1);
        listas.addAll(db.getListas(this));
        adaptador = new AdaptadorListas(this);
        ListView lista = findViewById(R.id.lista_listas);
        lista.setAdapter(adaptador);

        Button btn_add = findViewById(R.id.btn_add);
        btn_add.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), CrearListaActivity.class)));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        listas.clear();
        listas.addAll(db.getListas(this));
        adaptador.notifyDataSetChanged();
    }

    class AdaptadorListas extends ArrayAdapter {
        final Activity context;

        public AdaptadorListas(Activity context) {
            super(context, R.layout.listitem_lista, listas);
            this.context = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Lista lista = listas.get(position);
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.listitem_lista, parent, false);
            TextView lbl_lista = item.findViewById(R.id.lbl_lista);
            lbl_lista.setText(lista.getNombre());
            ImageButton btn_delete = item.findViewById(R.id.btn_delete);
            btn_delete.setOnClickListener(view -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.dialog_del_lista_message)
                        .setTitle(R.string.dialog_del_lista_title);
                builder.setPositiveButton(R.string.ok, (dialog, id) -> {
                    db.deleteLista(lista.getId());
                    listas.remove(lista);
                    adaptador.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(), R.string.toast_del_lista, Toast.LENGTH_SHORT).show();
                });
                builder.setNegativeButton(R.string.cancel, (dialog, id) -> {
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            });
            Button btn_arrow = item.findViewById(R.id.btn_arrow_lista);
            btn_arrow.setOnClickListener(view -> {
                Intent intent = new Intent(getApplicationContext(), CancionesListaActivity.class);
                intent.putExtra("lista", lista);
                intent.putExtra("position", position);
                startActivity(intent);
            });
            return item;
        }
    }
}