package com.example.reproductorabril.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reproductorabril.R;
import com.example.reproductorabril.modelo.Cancion;

import java.util.ArrayList;
import java.util.List;

public class CancionesActivity extends AppCompatActivity {

    List<Cancion> canciones = new ArrayList<>();
    AdaptadorCancion adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canciones);
        Bundle ex = getIntent().getExtras();
        canciones = ex.getParcelableArrayList("canciones");
        adaptador = new AdaptadorCancion(this);
        ListView lista = findViewById(R.id.lista_canciones);
        lista.setAdapter(adaptador);
    }

    class AdaptadorCancion extends ArrayAdapter {
        final Activity context;

        public AdaptadorCancion(Activity context) {
            super(context, R.layout.listitem_cancion, canciones);
            this.context = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Cancion cancion = canciones.get(position);
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.listitem_cancion, parent, false);
            if (!cancion.getNombre().isEmpty()) {
                TextView lblCancion = item.findViewById(R.id.lbl_cancion);
                lblCancion.setText(cancion.getNombre());
            }
            if (!cancion.getArtista().isEmpty()) {
                TextView lblArtistas = item.findViewById(R.id.lbl_artistas);
                lblArtistas.setText(cancion.getArtista());
            }
            if (!cancion.getAlbum().isEmpty()) {
                TextView lblAlbum = item.findViewById(R.id.lbl_album);
                lblAlbum.setText(cancion.getAlbum());
            }
            if (cancion.getCover() != null) {
                ImageView cover = item.findViewById(R.id.song_icon);
                cover.setImageBitmap(cancion.getCover());
                cover.setPadding(0, 0, 0, 0);
                cover.setClipToOutline(true);
            }
            Button btn_arrow = item.findViewById(R.id.btn_arrow);
            btn_arrow.setOnClickListener(view -> {
                Intent intent = new Intent(getApplicationContext(), PlayActivity.class);
                intent.putExtra("cancion", cancion);
                intent.putParcelableArrayListExtra("canciones", (ArrayList<? extends Parcelable>) canciones);
                intent.putExtra("position", position);
                intent.putExtra("lista", 0);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(context, new Pair<>(item, "viewFullSong"),
                            new Pair<>(item.findViewById(R.id.lbl_cancion), "play_cancion"),
                            new Pair<>(item.findViewById(R.id.lbl_artistas), "play_artistas"),
                            new Pair<>(item.findViewById(R.id.lbl_album), "play_album"),
                            new Pair<>(findViewById(R.id.song_icon), "play_song_icon"));
                    startActivity(intent, options.toBundle());
                } else
                    startActivity(intent);
            });
            return item;
        }
    }

}