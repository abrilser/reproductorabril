package com.example.reproductorabril.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.reproductorabril.R;
import com.example.reproductorabril.db.DBHelper;
import com.example.reproductorabril.db.Helper;
import com.example.reproductorabril.modelo.Cancion;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Cancion> canciones = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 23);

        DBHelper db = new DBHelper(this, "reproductor", null, 1);

        Button btn_canciones = findViewById(R.id.btn_inicio_canciones);
        btn_canciones.setOnClickListener(view -> {
            canciones.clear();
            loadSongs();
            Intent intent = new Intent(getApplicationContext(), CancionesActivity.class);
            intent.putParcelableArrayListExtra("canciones", (ArrayList<? extends Parcelable>) canciones);
            startActivity(intent);
        });

        Button btn_listas = findViewById(R.id.btn_inicio_listas);
        btn_listas.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(), ListasActivity.class));
        });

        Button btn_play = findViewById(R.id.btn_inicio_play);
        btn_play.setOnClickListener(view -> {
            SharedPreferences sharedPref = getSharedPreferences("key", MODE_PRIVATE);
            int position = sharedPref.getInt("position", 0);
            long lista = sharedPref.getLong("lista", 0);
            canciones.clear();
            if (lista == 0) {
                loadSongs();
            } else {
                canciones.addAll(db.getLista(this, lista).getCanciones());
            }
            if (canciones.size() > 0) {
                int pos = position >= canciones.size() ? 0 : position;
                Intent intent = new Intent(getApplicationContext(), PlayActivity.class);
                intent.putParcelableArrayListExtra("canciones", (ArrayList<? extends Parcelable>) canciones);
                intent.putExtra("position", pos);
                intent.putExtra("cancion", canciones.get(pos));
                intent.putExtra("lista", lista);
                startActivity(intent);
            } else {
                Toast.makeText(this, "No hay canciones disponibles para reproducir", Toast.LENGTH_SHORT).show();
            }
        });

        Button btn_settings = findViewById(R.id.btn_settings);
        btn_settings.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), PreferencesActivity.class)));

        Button btn_about = findViewById(R.id.btn_about);
        btn_about.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.dialogstyle);
            builder.setMessage(R.string.about_message)
                    .setTitle(R.string.about_title);
            builder.setNegativeButton(R.string.close, (dialog, id) -> {
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }

    private void loadSongs() {
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String sele = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        String[] proj = {MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ALBUM_ID};
        Cursor cursor = getContentResolver().query(uri, proj, sele, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String author = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                String durationFormat = Helper.setCorrectDuration(duration);
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                Cancion cancion = new Cancion(name, author, album, durationFormat, data);
                long albumId = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID));
                cancion.setCover(Helper.getAlbumart(this, albumId));
                canciones.add(cancion);
            }
            cursor.close();
        }
    }

    private void CopyRAWtoPhone(int id, String path) throws IOException {
        InputStream in = getResources().openRawResource(id);
        FileOutputStream out = new FileOutputStream(path);
        byte[] buff = new byte[1024];
        int read = 0;
        try {
            while ((read = in.read(buff)) > 0) {
                out.write(buff, 0, read);
            }
        } finally {
            in.close();
            out.close();
        }
    }
}