package com.example.reproductorabril.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reproductorabril.R;
import com.example.reproductorabril.db.DBHelper;
import com.example.reproductorabril.modelo.Cancion;
import com.example.reproductorabril.modelo.Lista;

import java.util.ArrayList;
import java.util.List;

public class CancionesListaActivity extends AppCompatActivity {

    List<Cancion> canciones = new ArrayList<>();
    AdaptadorCancionLista adaptador;
    Lista listarep;
    DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canciones_lista);
        db = new DBHelper(this, "reproductor", null, 1);
        Bundle ex = getIntent().getExtras();
        listarep = ex.getParcelable("lista");
        canciones = listarep.getCanciones();
        TextView titulo_lista = findViewById(R.id.titulo_lista);
        titulo_lista.setText(listarep.getNombre());
        adaptador = new AdaptadorCancionLista(this);
        ListView lista = findViewById(R.id.lista_canciones2);
        lista.setAdapter(adaptador);
    }

    class AdaptadorCancionLista extends ArrayAdapter {
        final Activity context;

        public AdaptadorCancionLista(Activity context) {
            super(context, R.layout.listitem_cancion2, canciones);
            this.context = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Cancion cancion = canciones.get(position);
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.listitem_cancion2, parent, false);
            if (!cancion.getNombre().isEmpty()) {
                TextView lblCancion = item.findViewById(R.id.lbl_cancion2);
                lblCancion.setText(cancion.getNombre());
            }
            if (!cancion.getArtista().isEmpty()) {
                TextView lblArtistas = item.findViewById(R.id.lbl_artistas2);
                lblArtistas.setText(cancion.getArtista());
            }
            if (!cancion.getAlbum().isEmpty()) {
                TextView lblAlbum = item.findViewById(R.id.lbl_album2);
                lblAlbum.setText(cancion.getAlbum());
            }
            if (cancion.getCover() != null) {
                ImageView cover = item.findViewById(R.id.song_icon2);
                cover.setImageBitmap(cancion.getCover());
                cover.setPadding(0, 0, 0, 0);
                cover.setClipToOutline(true);
            }
            ImageButton btn_delete = item.findViewById(R.id.btn_delete2);
            btn_delete.setOnClickListener(view -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.dialog_del_cancion_message)
                        .setTitle(R.string.dialog_del_cancion_title);
                builder.setPositiveButton(R.string.ok, (dialog, id) -> {
                    db.deleteCancion(cancion.getUri(), listarep.getId());
                    canciones.remove(cancion);
                    adaptador.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(), R.string.toast_del_cancion, Toast.LENGTH_SHORT).show();
                });
                builder.setNegativeButton(R.string.cancel, (dialog, id) -> {
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            });
            Button btn_arrow = item.findViewById(R.id.btn_arrow2);
            btn_arrow.setOnClickListener(view -> {
                Intent intent = new Intent(getApplicationContext(), PlayActivity.class);
                intent.putExtra("cancion", cancion);
                intent.putParcelableArrayListExtra("canciones", (ArrayList<? extends Parcelable>) canciones);
                intent.putExtra("position", position);
                intent.putExtra("lista", listarep.getId());
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(context, new Pair<>(item, "viewFullSong"),
                            new Pair<>(item.findViewById(R.id.lbl_cancion), "play_cancion"),
                            new Pair<>(item.findViewById(R.id.lbl_artistas), "play_artistas"),
                            new Pair<>(item.findViewById(R.id.lbl_album), "play_album"),
                            new Pair<>(findViewById(R.id.song_icon), "play_song_icon"));
                    startActivity(intent, options.toBundle());
                } else
                    startActivity(intent);
            });
            return item;
        }
    }
}