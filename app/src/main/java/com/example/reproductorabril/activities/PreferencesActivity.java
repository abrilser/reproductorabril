package com.example.reproductorabril.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.reproductorabril.R;

public class PreferencesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        if (findViewById(R.id.frameSettings) != null) {
            if (savedInstanceState != null) {
                return;
            }
            getFragmentManager().beginTransaction().add(R.id.frameSettings, new SettingsFragment()).commit();
        }
    }
}