package com.example.reproductorabril.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Lista implements Parcelable {

    public static final Creator<Lista> CREATOR = new Creator<Lista>() {
        @Override
        public Lista createFromParcel(Parcel in) {
            return new Lista(in);
        }

        @Override
        public Lista[] newArray(int size) {
            return new Lista[size];
        }
    };

    private long id;
    private String nombre;
    private List<Cancion> canciones;

    public Lista(String nombre) {
        this.nombre = nombre;
        this.canciones = new ArrayList<>();
    }

    protected Lista(Parcel in) {
        id = in.readLong();
        nombre = in.readString();
        canciones = in.createTypedArrayList(Cancion.CREATOR);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Cancion> getCanciones() {
        return canciones;
    }

    public void setCanciones(List<Cancion> canciones) {
        this.canciones = canciones;
    }

    private void ordenAleatorio() {
        Collections.shuffle(canciones);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(nombre);
        dest.writeTypedList(canciones);
    }
}
