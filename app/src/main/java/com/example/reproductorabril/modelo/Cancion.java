package com.example.reproductorabril.modelo;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Cancion implements Parcelable {

    public static final Creator<Cancion> CREATOR = new Creator<Cancion>() {
        @Override
        public Cancion createFromParcel(Parcel in) {
            return new Cancion(in);
        }

        @Override
        public Cancion[] newArray(int size) {
            return new Cancion[size];
        }
    };
    private String nombre;
    private String artista;
    private String album;
    private String duracion;
    private String uri;
    private Bitmap cover;

    public Cancion() {

    }

    public Cancion(String nombre, String artista, String album, String duracion, String uri) {
        this.nombre = nombre;
        this.artista = artista;
        this.album = album;
        this.duracion = duracion;
        this.uri = uri;
    }

    protected Cancion(Parcel in) {
        nombre = in.readString();
        artista = in.readString();
        album = in.readString();
        duracion = in.readString();
        uri = in.readString();
        cover = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Bitmap getCover() {
        return cover;
    }

    public void setCover(Bitmap cover) {
        this.cover = cover;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeString(artista);
        dest.writeString(album);
        dest.writeString(duracion);
        dest.writeString(uri);
        dest.writeParcelable(cover, flags);
    }
}
