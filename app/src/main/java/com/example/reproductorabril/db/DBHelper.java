package com.example.reproductorabril.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.reproductorabril.modelo.Cancion;
import com.example.reproductorabril.modelo.Lista;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    public static final String TABLE_LISTA = "listas";
    public static final String LISTA_COLUMN_ID = "id_lista";
    public static final String LISTA_COLUMN_NOMBRE = "nombre";

    public static final String TABLE_CANCION = "canciones";
    public static final String CANCION_COLUMN_ID = "id_cancion";
    public static final String CANCION_COLUMN_RUTA = "uri";
    public static final String CANCION_COLUMN_LISTA = LISTA_COLUMN_ID;

    public static final String CREATE_PK = " INTEGER PRIMARY KEY AUTOINCREMENT,";

    public static final String CREATE_LISTA =
            "CREATE TABLE " + TABLE_LISTA + "("
                    + LISTA_COLUMN_ID + CREATE_PK
                    + LISTA_COLUMN_NOMBRE + " TEXT"
                    + ")";

    public static final String CREATE_CANCION =
            "CREATE TABLE " + TABLE_CANCION + "("
                    + CANCION_COLUMN_ID + CREATE_PK
                    + CANCION_COLUMN_RUTA + " TEXT,"
                    + CANCION_COLUMN_LISTA + " INTEGER,"
                    + "CONSTRAINT cancion_lista UNIQUE (" + CANCION_COLUMN_LISTA + "," + CANCION_COLUMN_RUTA + "), "
                    + "FOREIGN KEY(" + CANCION_COLUMN_LISTA + ") "
                    + "REFERENCES " + TABLE_LISTA + "(" + LISTA_COLUMN_ID + ")"
                    + ")";

    private String sql;

    public DBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_LISTA);
        db.execSQL(CREATE_CANCION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CANCION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LISTA);
        onCreate(db);
    }

    public Lista createLista(String nombre) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LISTA_COLUMN_NOMBRE, nombre);
        long id = db.insert(TABLE_LISTA, null, values);
        Lista lista = new Lista(nombre);
        lista.setId(id);
        db.close();
        return lista;
    }

    public void deleteLista(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("id", id + "");
        db.delete(TABLE_CANCION, CANCION_COLUMN_LISTA + " = ?", new String[]{String.valueOf(id)});
        db.delete(TABLE_LISTA, LISTA_COLUMN_ID + " = ?", new String[]{String.valueOf(id)});
        db.close();
    }

    public void deleteCancion(String uri, long lista_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CANCION, CANCION_COLUMN_RUTA + " = ? AND " + CANCION_COLUMN_LISTA + " = ?", new String[]{uri, String.valueOf(lista_id)});
        db.close();
    }

    public void addCancionToLista(String uri, long lista_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CANCION_COLUMN_LISTA, lista_id);
        values.put(CANCION_COLUMN_RUTA, uri);
        db.insert(TABLE_CANCION, null, values);
        db.close();
    }

    public List<Lista> getListas(Context ctx) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Lista> listas = new ArrayList<>();
        sql = "SELECT * FROM " + TABLE_LISTA;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                List<Cancion> canciones = new ArrayList<>();
                String sub = "SELECT * FROM " + TABLE_CANCION + " WHERE " + CANCION_COLUMN_LISTA + " = " + cursor.getLong(0);
                Cursor subcur = db.rawQuery(sub, null);
                if (subcur.moveToFirst()) {
                    do {
                        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        String sele = MediaStore.Audio.Media.IS_MUSIC + " != 0";
                        String[] proj = {MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ALBUM_ID};
                        Cursor cancur = ctx.getContentResolver().query(uri, proj, sele, null, null);
                        if (cancur != null) {
                            while (cancur.moveToNext()) {
                                String name = cancur.getString(cancur.getColumnIndex(MediaStore.Audio.Media.TITLE));
                                String author = cancur.getString(cancur.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                                String album = cancur.getString(cancur.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                                String duration = cancur.getString(cancur.getColumnIndex(MediaStore.Audio.Media.DURATION));
                                String durationFormat = Helper.setCorrectDuration(duration);
                                String data = cancur.getString(cancur.getColumnIndex(MediaStore.Audio.Media.DATA));
                                if (data.equals(subcur.getString(1))) {
                                    Cancion cancion = new Cancion(name, author, album, durationFormat, data);
                                    long albumId = cancur.getLong(cancur.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID));
                                    cancion.setCover(Helper.getAlbumart(ctx, albumId));
                                    canciones.add(cancion);
                                    break;
                                }
                            }
                            cancur.close();
                        }
                    } while (subcur.moveToNext());
                }
                subcur.close();
                Lista lista = new Lista(cursor.getString(1));
                lista.setCanciones(canciones);
                lista.setId(cursor.getLong(0));
                listas.add(lista);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listas;
    }

    public Lista getLista(Context ctx, long lista_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Lista lista = null;
        sql = "SELECT * FROM " + TABLE_LISTA + " WHERE " + LISTA_COLUMN_ID + " = " + lista_id;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            List<Cancion> canciones = new ArrayList<>();
            String sub = "SELECT * FROM " + TABLE_CANCION + " WHERE " + CANCION_COLUMN_LISTA + " = " + cursor.getLong(0);
            Cursor subcur = db.rawQuery(sub, null);
            if (subcur.moveToFirst()) {
                do {
                    Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    String sele = MediaStore.Audio.Media.IS_MUSIC + " != 0";
                    String[] proj = {MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ALBUM_ID};
                    Cursor cancur = ctx.getContentResolver().query(uri, proj, sele, null, null);
                    if (cancur != null) {
                        while (cancur.moveToNext()) {
                            String name = cancur.getString(cancur.getColumnIndex(MediaStore.Audio.Media.TITLE));
                            String author = cancur.getString(cancur.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                            String album = cancur.getString(cancur.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                            String duration = cancur.getString(cancur.getColumnIndex(MediaStore.Audio.Media.DURATION));
                            String durationFormat = Helper.setCorrectDuration(duration);
                            String data = cancur.getString(cancur.getColumnIndex(MediaStore.Audio.Media.DATA));
                            if (data.equals(subcur.getString(1))) {
                                Cancion cancion = new Cancion(name, author, album, durationFormat, data);
                                long albumId = cancur.getLong(cancur.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID));
                                cancion.setCover(Helper.getAlbumart(ctx, albumId));
                                canciones.add(cancion);
                                break;
                            }
                        }
                        cancur.close();
                    }
                } while (subcur.moveToNext());
            }
            subcur.close();
            lista = new Lista(cursor.getString(1));
            lista.setCanciones(canciones);
        }
        cursor.close();
        db.close();
        return lista;
    }
}
