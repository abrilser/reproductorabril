package com.example.reproductorabril.db;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import java.io.FileDescriptor;

public class Helper {

    public static Bitmap getAlbumart(Context ctx, Long album_id) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        try {
            final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(sArtworkUri, album_id);
            ParcelFileDescriptor pfd = ctx.getContentResolver().openFileDescriptor(uri, "r");
            if (pfd != null) {
                FileDescriptor fd = pfd.getFileDescriptor();
                bm = BitmapFactory.decodeFileDescriptor(fd, null, options);
            }
        } catch (Error ee) {
        } catch (Exception e) {
        }
        return bm;
    }

    public static String setCorrectDuration(String songs_duration) {
        if (Integer.valueOf(songs_duration) != null) {
            int time = Integer.valueOf(songs_duration);
            int seconds = time / 1000;
            int minutes = seconds / 60;
            seconds = seconds % 60;
            if (seconds < 10) {
                songs_duration = minutes + ":0" + seconds;
            } else {
                songs_duration = minutes + ":" + seconds;
            }
            return songs_duration;
        }
        return null;
    }
}
